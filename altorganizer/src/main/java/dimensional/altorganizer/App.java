package dimensional.altorganizer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class App {
	public static void main(String[] args) throws IOException {
		URL url = new URL(
				"http://eu.battle.net/api/wow/guild/Grim-Batol/Dimensional?fields=members");
		InputStream is = url.openStream();
		JsonReader rdr = Json.createReader(is);

		JsonObject guild = rdr.readObject();
		rdr.close();
		is.close();
		
		JsonArray members = guild.getJsonArray("members");
		
		guild = null;

		HashMap<Integer, ArrayList<JsonObject>> membersSortedOnRanks = new HashMap<Integer, ArrayList<JsonObject>>();

		// Convert JsonArray to HashMap to be able to select on ranks
		for (JsonObject member : members.getValuesAs(JsonObject.class)) {
			int rank = member.getJsonNumber("rank").intValue();

			if (!membersSortedOnRanks.containsKey(rank)) {
				membersSortedOnRanks.put(rank, new ArrayList<JsonObject>());
			}

			membersSortedOnRanks.get(rank).add(member);
		}

		members = null;

		ArrayList<JsonObject> raiders = new ArrayList<JsonObject>();
		
		for (JsonObject raider : membersSortedOnRanks.get(4)) {
			raiders.add(raider);
		}

		ArrayList<JsonObject> alts = new ArrayList<JsonObject>();

		for (JsonObject alt : membersSortedOnRanks.get(9)) {
			if (Roster.containsAlt(alt.getJsonObject("character").getString(
					"name"))) {
				alts.add(alt);
			}
		}

	}
}
