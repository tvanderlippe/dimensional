package dimensional.altorganizer;

import java.util.HashMap;
import java.util.Map.Entry;

public class Roster {

	private static final HashMap<String, String[]> ROSTER = new HashMap<String, String[]>();

	static {
		ROSTER.put("Jeremybell", new String[] { "Mollybell", "Henrybell" });
	}
	
	public static boolean containsAlt(String alt) {
		for (Entry<String, String[]> raider : ROSTER.entrySet()) {
			for (String currentAlt : raider.getValue()) {
				if (currentAlt.equals(alt)) {
					return true;
				}
			}
		}
		
		return false;
	}

}
